const app = new Vue({
    el: '#app',
    data: {
        titulo: 'Hola mundo con Vue',
        frutas: [
            {
                nombre: 'Durazno',
                cantidad: 10
            }, {
                nombre: 'Manzana',
                cantidad: 0
            }, {
                nombre: 'Platano',
                cantidad: 11
            }
        ],
        nuevaFruta: ''
    },
    methods: {
        agregarFruta() {
            this.frutas.push({nombre: this.nuevaFruta, cantidad: 0});
        }
    }
});

var barProgress = new Vue({
    el: '#barProgress',
    data: {
        dpProgress: 6,
        interval: ''
    },
    mounted: function () {
        this.$refs['progreso'].style.width = this.dpProgress + '%';
        document.getElementById('progreso').className = this.dpProgress < 50 ? 'progress-bar bg-danger' : 'progress-bar bg-primary';
        this.interval = setInterval(() => {
            this.dpProgress = this.dpProgress + 1;
        }, 300);
    },
    updated: function () {
        this.$refs['progreso'].style.width = this.dpProgress + '%';
        document.getElementById('progreso').className = this.dpProgress < 50 ? 'progress-bar bg-danger' : 'progress-bar bg-primary';
        if (this.dpProgress > 99) {
            clearInterval(this.interval);
        }
    }
});
var app2 = new Vue({
    el: '#app2a',
    data: {
        pdMensaje: 'Mensaje a mostrar en el DOM del HTMLz',
        pdMostrar: true
    }
});

var horarios = new Vue({
    el: '#horarios',
    data: {
        pdMostrar: true
    }
});
var seleccion = new Vue({
    el: '#seleccion',
    data: {

        pdSelected: ''
    }
});
var seleccionq = new Vue({
    el: '#seleccionq',
    data: {
        pdSelected: '',
        pdDeportes: [
            'Fútbol',
            'Béisbol',
            'Golf',
            'Baloncesto',
            'Tenis'
        ]
    }
});

var appa = new Vue({
    el: '#appa',
    data: {
        pdMensajee: 'Mensaje a colocar en el DOM del HTML'
    }
});
var appe = new Vue({
    el: '#appez',
    data: {
        pdMensajez: 'Mensaje a colocar en el DOM del HTMLz'
    }
});
var apponce = new Vue({
    el: '#apponce',
    data: {
        pdMensaje: 'Mensaje a colocar en el DOM del HTMLaa'
    }
});

var htmlElement = new Vue({
    el: '#htmlTag',
    data: {
        htmlElement: '<p>Mensaje a colocar en el <b>DOM</b> de la página usando la directiva <code>v-html</code> de Vue</p>'
    }
});
var htmlAttr = new Vue({
    el: '#htmlAttr',
    data: {
        twitterAddr: 'https://twitter.com/nextu',
        iconAddr: 'https://image.flaticon.com/icons/svg/145/145812.svg'
    }
});
var barProgressz = new Vue({
    el: '#barProgressz',
    data: {
        dpProgress: 80
    }
});
var logon = new Vue({
    el: '#login',
    data: {
        pdTitulo: 'Ingreso al Sistema',
        pdCampo1: 'Correo Electrónico',
        pdCampo2: 'Contraseña',
        logon: false,
        pdEmail: ''
    }
});

// CRONOMETRO
var crono = new Vue({
    el: '#crono',
    data: {
        pdTiempo: 0,
        interval: '',
        pdHoras: 0,
        pdMinutos: 0,
        pdSegundos: 0,
        textColor: 'info',
        tiempoInicial: 63 // Medido en segundos
    },
    mounted: function () {
        const valorInicial = this.tiempoInicial;
        this.pdSegundos = Math.floor(this.tiempoInicial % 60);
        const minutos = Math.floor(this.tiempoInicial / 60);
        this.pdMinutos = minutos < 60 ? minutos : Math.floor(this.tiempoInicial % 60);
        const horas = this.tiempoInicial / 3600;
        this.pdHoras = Math.floor(horas);
        this.interval = setInterval(() => {
            this.pdTiempo = this.pdTiempo + 1;
            if (this.pdTiempo > 9) {
                this.pdTiempo = 0;
                this.pdSegundos = this.pdSegundos - 1;
                this.pdSegundos = this.pdSegundos > 0 ? this.pdSegundos : 0;
                this.tiempoInicial = this.tiempoInicial - 1;
            }
            if (this.pdSegundos === 0) {
                this.pdMinutos = this.pdMinutos - 1;
                this.pdSegundos = this.tiempoInicial > 59 ? 59 : 0;
                this.pdMinutos = this.pdMinutos > 0 ? this.pdMinutos : 0;
            }
            if (this.pdMinutos === 0) {
                this.pdMinutos = this.pdHoras > 0 ? 59 : 0;
                this.pdHoras = this.pdHoras - 1;
                this.pdHoras = this.pdHoras > 0 ? this.pdHoras : 0;
            }
            if (this.tiempoInicial / valorInicial > 0.75) {
                this.textColor = 'info';
            } else if (this.tiempoInicial / valorInicial > 0.3) {
                this.textColor = 'primary';
            } else if (this.tiempoInicial / valorInicial > 0.10) {
                this.textColor = 'warning';
            } else {
                this.textColor = 'danger';
            }
            if (this.tiempoInicial === 0) {
                this.pdTiempo = 10;
                this.pdHoras = 0;
                this.pdMinutos = 0;
                this.pdSegundos = 0;
                clearInterval(this.interval);
            }
        }, 100);
    }
});
var appxa = new Vue({
    el: '#appxa',
    data: {
        pdMensaje: 'Mensaje a mostrar en el DOM del HTML',
        pdMostrar: true
    }
});
var appda = new Vue({
    el: '#appda',
    data: {
        pdMensaje: 'Mensaje a mostrar en el DOM del HTML',
        pdMostrar: true
    }
});
var horarioss = new Vue({
    el: '#horarioss',
    data: {
        pdMostrar: true
    }
});

var seleccionz = new Vue({
    el: '#seleccionz',
    data: {

        pdSelected: ''
    }
});
var seleccionfor = new Vue({
    el: '#seleccionfor',
    data: {
        pdSelected: '',
        pdDeportes: [
            'Fútbol',
            'Béisbol',
            'Golf',
            'Baloncesto',
            'Tenis'
        ]
    }
});

// Eventos y Contadores
var contadora = new Vue({
    el: '#contadora',
    data: {
        cantidad: 0
    }
});
// cronometro

var crono = new Vue({
    el: '#cronno',
    data: {
        pdTiempo: 0,
        interval: '',
        pdHoras: 0,
        pdMinutos: 0,
        pdSegundos: 0
    },
    methods: {
        manejoClick: function (event) {
            if (event.target.id === 'iniciar') {
                this.interval = setInterval(() => {
                    this.pdTiempo = this.pdTiempo + 1;
                    if (this.pdTiempo > 9) {
                        this.pdTiempo = 0;
                        this.pdSegundos = this.pdSegundos + 1;
                    }
                    if (this.pdSegundos > 59) {
                        this.pdSegundos = 0;
                        this.pdMinutos = this.pdMinutos + 1;
                    }
                    if (this.pdMinutos > 59) {
                        this.pdMinutos = 0;
                        this.pdHoras = this.pdHoras + 1;
                    }
                }, 100);
            } else if (event.target.id === 'detener') {
                clearInterval(this.interval);
            }
        }
    }
});


// Modificadores de eventos
var logozn = new Vue({
    el: '#loginn',
    data: {
        pdTitulo: 'Ingreso al Sistema',
        pdCampo1: 'Correo Electrónico',
        pdCampo2: 'Contraseña',
        logon: false,
        pdEmail: '',
        pdPass: ''
    },
    methods: {
        changeFocus: function (e) {
            if (e.target.id === 'pdEmail') {
                this.$refs.pdPass.focus();
            } else if (e.target.id === 'pdPass') {
                this.validateLogin();
            }
        },
        validateLogin: function () {
            if (this.pdEmail.length > 0 && this.pdPass.length > 0) {
                this.logon = true;
            } else {
                alert('Usuario o Contraseña inválidos');
            }
        }
    }
});

var crono = new Vue({
    el: '#cronos',
    data: {
        pdTiempo: 0,
        interval: '',
        pdHoras: 0,
        pdMinutos: 0,
        pdSegundos: 0,
        textColor: 'info',
        valorInicial: 68 // Medido en segundos
    },
    mounted: function () {
        this.tiempoInicial = this.valorInicial;
        this.pdSegundos = Math.floor(this.tiempoInicial % 60);
        const minutos = Math.floor(this.tiempoInicial / 60);
        this.pdMinutos = minutos < 60 ? minutos : Math.floor(this.tiempoInicial % 60);
        const horas = this.tiempoInicial / 3600;
        this.pdHoras = Math.floor(horas);
    },
    methods: {
        manejoClick: function (evento) {
            if (evento.target.id === 'iniciar') {
                if (this.tiempoInicial > 0) {
                    this.interval = setInterval(() => {
                        this.pdTiempo = this.pdTiempo + 1;
                        if (this.pdTiempo > 9) {
                            this.pdTiempo = 0;
                            this.pdSegundos = this.pdSegundos - 1;
                            this.pdSegundos = this.pdSegundos > 0 ? this.pdSegundos : 0;
                            this.tiempoInicial = this.tiempoInicial - 1;
                        }
                        if (this.pdSegundos === 0) {
                            this.pdMinutos = this.pdMinutos - 1;
                            this.pdSegundos = this.tiempoInicial > 59 ? 59 : 0;
                            this.pdMinutos = this.pdMinutos > 0 ? this.pdMinutos : 0;
                        }
                        if (this.pdMinutos === 0) {
                            this.pdMinutos = this.pdHoras > 0 ? 59 : 0;
                            this.pdHoras = this.pdHoras - 1;
                            this.pdHoras = this.pdHoras > 0 ? this.pdHoras : 0;
                        }
                        if (this.tiempoInicial / this.valorInicial > 0.75) {
                            this.textColor = 'info';
                        } else if (this.tiempoInicial / this.valorInicial > 0.3) {
                            this.textColor = 'primary';
                        } else if (this.tiempoInicial / this.valorInicial > 0.10) {
                            this.textColor = 'warning';
                        } else {
                            this.textColor = 'danger';
                        }
                        if (this.tiempoInicial === 0) {
                            this.pdTiempo = 10;
                            this.pdHoras = 0;
                            this.pdMinutos = 0;
                            this.pdSegundos = 0;
                            clearInterval(this.interval);
                        }
                    }, 100);
                }
            } else {
                clearInterval(this.interval);
            }
        },
        manejoTeclado: function (evento) {
            this.tiempoInicial = this.valorInicial;
            this.pdSegundos = Math.floor(this.tiempoInicial % 60);
            const minutos = Math.floor(this.tiempoInicial / 60);
            this.pdMinutos = minutos < 60 ? minutos : Math.floor(this.tiempoInicial % 60);
            const horas = this.tiempoInicial / 3600;
            this.pdHoras = Math.floor(horas);
            this.pdTiempo = 10;
        }
    }
});

var logoni = new Vue({
    el: '#logini',
    data: {
        pdTitulo: 'Ingreso al Sistema',
        pdCampo1: 'Correo Electrónico',
        pdCampo2: 'Contraseña',
        logon: false,
        pdEmail: '',
        pdPass: '',
        valid: ''
    },
    methods: {
        changeFocus: function (e) {
            if (e.target.id === 'pdEmail') {
                this.$refs.pdPass.focus();
            } else if (e.target.id === 'pdPass') {
                this.validateLogin();
            }
        },
        validateLogin: function () {
            if (this.emailValido(this.pdEmail) && this.passValido(this.pdPass)) {
                this.logon = true;
            } else {
                alert('Usuario o Contraseña inválidos');
            }
        },
        emailValido: function (email) {
            reEmail = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/;
            return reEmail.test(email);
        },
        passValido: function (pass) {
            rePass = /.{3,14}/;
            return rePass.test(pass);
        },
        manejaChange: function (e) {
            if (e.target.id === 'pdEmail') {
                this.valid = this.emailValido(this.pdEmail) ? 'is-valid' : 'is-invalid';
            } else if (e.target.id === 'pdPass') {
                this.validateLogin();
            }
        }
    }
});

var cronobt = new Vue({
    el: '#cronoxz',
    data: {
        valido: '',
        pdTiempo: 0,
        interval: '',
        pdHoras: 0,
        pdMinutos: 0,
        pdSegundos: 0,
        textColor: 'info',
        valorInicial: 68 // Medido en segundos
    },
    mounted: function () {
        this.tiempoInicial = this.valorInicial;
        this.pdSegundos = Math.floor(this.tiempoInicial % 60);
        const minutos = Math.floor(this.tiempoInicial / 60);
        this.pdMinutos = minutos < 60 ? minutos : Math.floor(this.tiempoInicial % 60);
        const horas = this.tiempoInicial / 3600;
        this.pdHoras = Math.floor(horas);
    },
    methods: {
        manejoClick: function (evento) {
            if (evento.target.id === 'iniciar') {
                if (this.tiempoInicial > 0) {
                    this.interval = setInterval(() => {
                        this.pdTiempo = this.pdTiempo + 1;
                        if (this.pdTiempo > 9) {
                            this.pdTiempo = 0;
                            this.pdSegundos = this.pdSegundos - 1;
                            this.pdSegundos = this.pdSegundos > 0 ? this.pdSegundos : 0;
                            this.tiempoInicial = this.tiempoInicial - 1;
                        }
                        if (this.pdSegundos === 0) {
                            this.pdMinutos = this.pdMinutos - 1;
                            this.pdSegundos = this.tiempoInicial > 59 ? 59 : 0;
                            this.pdMinutos = this.pdMinutos > 0 ? this.pdMinutos : 0;
                        }
                        if (this.pdMinutos === 0) {
                            this.pdMinutos = this.pdHoras > 0 ? 59 : 0;
                            this.pdHoras = this.pdHoras - 1;
                            this.pdHoras = this.pdHoras > 0 ? this.pdHoras : 0;
                        }
                        if (this.tiempoInicial / this.valorInicial > 0.75) {
                            this.textColor = 'info';
                        } else if (this.tiempoInicial / this.valorInicial > 0.3) {
                            this.textColor = 'primary';
                        } else if (this.tiempoInicial / this.valorInicial > 0.10) {
                            this.textColor = 'warning';
                        } else {
                            this.textColor = 'danger';
                        }
                        if (this.tiempoInicial === 0) {
                            this.pdTiempo = 10;
                            this.pdHoras = 0;
                            this.pdMinutos = 0;
                            this.pdSegundos = 0;
                            clearInterval(this.interval);
                        }
                    }, 100);
                }
            } else {
                clearInterval(this.interval);
            }
        },
        manejoTeclado: function (evento) {
            if (!isNaN(evento.target.value)) {
                this.valido = 'is-valid';
                this.tiempoInicial = this.valorInicial;
                this.pdSegundos = Math.floor(this.tiempoInicial % 60);
                const minutos = Math.floor(this.tiempoInicial / 60);
                this.pdMinutos = minutos < 60 ? minutos : Math.floor(this.tiempoInicial % 60);
                const horas = this.tiempoInicial / 3600;
                this.pdHoras = Math.floor(horas);
                this.pdTiempo = 10;
            } else {
                this.valido = 'is-invalid';
            }
        }
    }
});

// Creando componentes en Vue

Vue.component('tarea-component', {
    template: '#tarea',
    data: function () {
        return {numero: 11, tarea: 'Estudiar componentes en Vue', agregar: 'fa fa-plus', eliminar: 'fa fa-minus'};
    }
});
new Vue({el: '#appqa'});

Vue.component('libros-componente', {
    template: '#libro',
    props: ['libro'],
    data: function () {
        return {test: 'test'};
    }
});

// Lista de libros a leer
var appv = new Vue({
    el: '#appv',
    data: {
        libros: [
            {
                nombre: 'Learning Vue.js 2',
                autor: 'Nathan Wu '
            }, {
                nombre: 'The Vue Handbook',
                autor: 'Flavio Copes '
            }, {
                nombre: 'Fullstack Vue',
                autor: 'Hassan Djirdeh'
            }
        ],
        nombreLibro: '',
        autorLibro: ''
    },
    methods: {
        manejarClick: function (evento) {
            if (evento.target.id === 'agregar') {
                this.libros.push({nombre: this.nombreLibro, autor: this.autorLibro});
                this.nombreLibro = '';
                this.autorLibro = '';
            }
        }
    }
});

// Lista de tareas
Vue.component('tarea-component', {
    template: '#tarea',
    data: function () {
        return {agregar: 'fa fa-plus', eliminar: 'fa fa-minus'};
    },
    props: ['numero', 'tarea']
});


var appt = new Vue({
    el: '#appt',
    data: {
        tasks: ['Estudiar componentes en Vue', 'Estudiar las props de los componentes', 'Hacer actividades con los componentes', 'Hacer Ejercicios con los componentes']
    }
});

// lista de tareas 2

Vue.component('tarea-component', {
    template: '#tareaa',
    data: function () {
        return {agregar: 'fa fa-plus', eliminar: 'fa fa-minus'};
    },
    props: ['numero', 'tarea', 'avance']
});

Vue.component('avance-componentt', {
    template: '#avancee',
    props: {
        progreso: Number
    }
});

var appp = new Vue({
    el: '#appp',
    data: {
        tasks: [
            {
                nombre: 'Estudiar componentes en Vue',
                avance: 27
            }, {
                nombre: 'Estudiar las props de los componentes',
                avance: 49
            }, {
                nombre: 'Hacer actividades con los componentes',
                avance: 93
            }, {
                nombre: 'Hacer Ejercicios con los componentes',
                avance: 16
            }
        ]
    }
});

// solución
Vue.component('libros-componente', {
    template: '#libroy',
    props: [
        'libro', 'indice'
    ],
    data: function () {
        return {test: 'test'};
    },
    methods: {
        manejarClick: function (evento) {
            if (evento.target.id === 'eliminarr') {
                app.$data.libros.splice(this.indice, 1);
            }
        }
    }
});


var apps = new Vue({
    el: '#apps',
    data: {
        libros: [
            {
                nombre: 'Learning Vue.js 2',
                autor: 'Nathan Wu '
            }, {
                nombre: 'The Vue Handbook',
                autor: 'Flavio Copes '
            }, {
                nombre: 'Fullstack Vue',
                autor: 'Hassan Djirdeh'
            }
        ],
        nombreLibro: '',
        autorLibro: ''
    },
    methods: {
        manejarClick: function (evento) {
            if (evento.target.id === 'agregar') {
                this.libros.push({nombre: this.nombreLibro, autor: this.autorLibro});
                this.nombreLibro = '';
                this.autorLibro = '';
            } else if (evento.target.id === 'eliminarr') {
                alert('eliminar');
            }
        }
    }
});
